const express = require('express');
const cors = require('cors');
const puppeteer = require('puppeteer');
const app = express();

app.use(express.json());
app.use(cors());

app.get('/', (req, res, next) => {
  res.status(200).send('OK');
})

app.post('/', (req, res, next) => {

  let browser;

  const HEADLESS = false;

  var search = req.body.searchText;
  var lat = req.body.latitude;
  var lng = req.body.longitude;

  (async () => {
    browser = await puppeteer.launch({ headless: true });

    const page = await browser.newPage();
    const url = `https://www.facebook.com/marketplace/delhi/search/?query=${search}&latitude=${lat}&longitude=${lng}&vertical=C2C&sort=BEST_MATCH`

    await page.goto(url);

    await page.waitFor('div[data-testid="marketplace_search_feed_content"]');

    const items = await page.evaluate(() => {
      const products = Array.from(document.querySelectorAll('div[data-testid="marketplace_search_feed_content"] > div > div:first-child > div')).map(product => {
        return {
          title: product.querySelector("p") ? product.querySelector('p').innerText : ''
        }
      });
      return products;
    });

    await browser.close();

    console.log(items);
    res.status(200).send(items);
  })();

})

// app.use(cors());

app.listen(3000, () => {
  console.log("Listening on 3000");
})